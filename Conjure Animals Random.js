/*
    TO MAKE IT WORK:
    -Add a DAE effect with a macro.itemMacro attribute key and @spellLevel as effect value
    -Set the spell target in the details as Self
    -If you want to let a player use this macro make sure he has the permissions to create Tokens and Actors
*/
const pack = game.packs.get("dnd5e.monsters");

const monsterType = "beast";
let castLevel = args[1];
let effectName = args[args.length - 1].efData.label;
let targetTokenId = args[args.length - 1].tokenId;
let target = canvas.tokens.get(targetTokenId);
let targetActor = target.actor;
let uEffectName = effectName + targetTokenId;

if (args[0] === "on") {
    let multiplier = castLevel < 5 ? 1 : (castLevel < 7 ? 2 : (castLevel < 9 ? 3 : 4));
    let contentCr = `<select id="cr"><option value="2">${multiplier * 1} CR2 ${monsterType}</option><option value="1">${multiplier * 2} CR1 ${monsterType}</option><option value="0.5">${multiplier * 4} CR1/2 ${monsterType}</option><option value="0.25">${multiplier * 8} CR1/4 ${monsterType}</option></select>`;
    let dialogCr = new Dialog({
        title: "Choose CR",
        content: contentCr,
        buttons: {
            yes: {
                icon: `<i class="fas fa-check"></i>`,
                label: "Confirm",
                callback: async (html) => {
                    let cr = parseFloat(html.find(`#cr`).val());
                    let numberOfTokens = (cr < 0.5 ? 8 : (cr < 1 ? 4 : (cr < 2 ? 2 : 1))) * multiplier;
                    await pack.getIndex({
                        fields: ["name", "data.details.type.value", "data.details.cr"]
                    });
                    let monsterList = pack.index.filter(i => i.data.details.type.value === monsterType && i.data.details.cr === cr);
                    let monsterName = monsterList[~~(Math.random() * monsterList.length)].name;
                    ui.notifications.notify(`You are summoning ${numberOfTokens} ${monsterName}`);
                    let monsterImportId = monsterList.find(i => i.name === monsterName)._id;
                    let actorToDelete = await game.actors.importFromCompendium(pack, monsterImportId, {
                        "name": monsterName + " summon",
                    });
                    let templateData = {
                        t: "rect",
                        user: game.user.id,
                        distance: 7.5 * actorToDelete.data.token.width,
                        direction: 45,
                        x: 0,
                        y: 0,
                        fillColor: game.user.color,
                        flags: {
                            Summon: {
                                effect: uEffectName
                            }
                        }
                    };
                    let doc = new CONFIG.MeasuredTemplate.documentClass(templateData, {
                        parent: canvas.scene
                    });
                    let template = new game.dnd5e.canvas.AbilityTemplate(doc);
                    template.actorSheet = targetActor.sheet;
                    let hooksRemaining = numberOfTokens;
                    hookAndSummon();
                    async function hookAndSummon() {
                        if (hooksRemaining > 0) {
                            Hooks.once("createMeasuredTemplate", hookAndSummon);
                            template.drawPreview();
                            hooksRemaining--;
                        } else {
                            let removeTemplates = canvas.templates.placeables.filter(i => i.data.flags.Summon?.effect === uEffectName);
                            let summonTokensData = [];
                            for (const coord of removeTemplates) {
                                let temp = duplicate(actorToDelete.data.token);
                                temp.x = coord.data.x;
                                temp.y = coord.data.y;
                                temp.name = monsterName + " summon";
                                temp.disposition = 1;
                                summonTokensData.push(temp);
                            }
                            let summonedTokens = await canvas.scene.createEmbeddedDocuments("Token", summonTokensData);
                            let summonedTokensIds = summonedTokens.map(function(w) {
                                return w.id;
                            });

                            DAE.setFlag(targetActor, uEffectName, {
                                tokensIds: summonedTokensIds,
                                monsterId: actorToDelete.id
                            });

                            let templateArray = removeTemplates.map(function(w) {
                                return w.id;
                            });
                            if (removeTemplates) await canvas.scene.deleteEmbeddedDocuments("MeasuredTemplate", templateArray);
                        }
                    }
                }
            }
        }
    }).render(true);
}

if (args[0] === "off") {
    let {
        tokensIds,
        monsterId
    } = DAE.getFlag(targetActor, uEffectName);
    canvas.scene.deleteEmbeddedDocuments("Token", tokensIds);
    game.actors.get(monsterId).delete();
    DAE.unsetFlag(targetActor, uEffectName);
}
