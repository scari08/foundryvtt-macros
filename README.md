# FoundryVTT Macros

_YOU NEED FOUNDRY VERSION 0.8.8, DAE, MIDIQOL AND ITEMMACRO MODULES TO MAKE THESE WORK_

## Conjure Macros
These macros let you choose the desired CR, as for the spell effect, and depending of the spell slot used it creates a number of summons. You can use the random version that randomly chooses a monsters that respects the criteria. Or use the pick version that lets you choose from the filtered list. The spell takes the data from the compendium (default: Monsters (SRD)) and it temporarily creates a modified actor in the Actor directory. Then it creates as many summons as needed and when the spell ends (or concentration is broken) it deletes the tokens AND the actor.

These spell macros look for actors directly on the compendium packs using the new more efficient indexing methods (added in foundry 0.8.8). You can reference any compendium (with Actor as type) in your world, you just need to change the first constant `pack` of the macro with the key of your compendium. You can see the key of your compendium opening the console (default F12) and entering the command `game.packs` and look for your compendium key in the list.

Example: I create a new compendium and I name it `test` and I set it as Actor. Opening the console and entering the command I can see in the list the key of the new compendium (it should be `world.test`). In the macro I replace the line of code `const pack = game.packs.get("dnd5e.monsters");` with `const pack = game.packs.get("world.test");`

![Alt text](https://gitlab.com/scari08/foundryvtt-macros/-/raw/main/Conjure%20Animals%20Example.gif)
