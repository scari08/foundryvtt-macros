// args[1] = @token, args[2] = @spellLevel
let summonName = "Fey Spirit"; //Change with the name of basic summon in your actorDirectory
let effectName = "summonFey";
let tokenSrcId = args[1];
let uEffectName = effectName + tokenSrcId;
let castLevel = args[2];
let target = canvas.tokens.get(tokenSrcId);
let targetActor = target.actor;


if(args[0] === "on") {
      
  let d = new Dialog({
    title: "Choose mood",
    content: `<form class="flexcol">
                <div class="form-group">
                    <select id="mood">
                        <option value="Fuming">Fuming</option>
                        <option value="Mirthful">Mirthful</option>
                        <option value="Tricksy">Tricksy</option>
                    </select>
                </div>
            </form>
            `,
    buttons: {
        yes: {
            icon: '<i class="fas fa-check"></i>',
            label: 'Yes',
            callback: async (html) => {
                let mood = html.find('#mood').val();
                let summonRange = await canvas.scene.createEmbeddedDocuments("MeasuredTemplate", [{
                    t: "circle",
                    user: game.user.id,
                    x: target.x + canvas.grid.size / 2,
                    y: target.y + canvas.grid.size / 2,
                    direction: 0,
                    distance: 60,
                    borderColor: "#FF0000",
                    flags: {
                        Summon: {
                            ActorId: targetActor.id
                        }
                    }
                }]);
                let templateData = {
                    t: "rect",
                    user: game.user.id,
                    distance: 7.5,
                    direction: 45,
                    x: 0,
                    y: 0,
                    fillColor: game.user.color,
                    flags: {
                        Summon: {
                            ActorId: targetActor.id
                        }
                    }
                };
                Hooks.once("createMeasuredTemplate", deleteTemplatesAndSummon);
                let doc = new CONFIG.MeasuredTemplate.documentClass(templateData, { parent: canvas.scene });
                let template = new game.dnd5e.canvas.AbilityTemplate(doc);
                template.actorSheet = targetActor.sheet;
                template.drawPreview();
                async function deleteTemplatesAndSummon(template){
                    let removeTemplates = canvas.templates.placeables.filter(i => i.data.flags.Summon?.ActorId === targetActor.id);
                    let templateArray = removeTemplates.map(function(w) {
                        return w.id;
                    });
                    let summonDataToken = duplicate(game.actors.find(i => i.name === summonName).data.token);
                    summonDataToken.x = template.data.x;
                    summonDataToken.y = template.data.y;
                    //you can change other token data here like name or img
                    let summonedToken = await canvas.scene.createEmbeddedDocuments("Token", [summonDataToken]);
                    let summonedTokenId = summonedToken[0].id;
                    DAE.setFlag(targetActor, uEffectName, summonedTokenId);
                    let summonedTokenActor = canvas.tokens.get(summonedTokenId).actor;
                    let itemMoods = summonedTokenActor.items.filter(i => i.name.includes("Fey Step") && !i.name.includes(mood));
                    let itemMoodsIds = itemMoods.map(function(w) {
                        return w.id;
                    });
                    await summonedTokenActor.deleteEmbeddedDocuments("Item", itemMoodsIds);
                    let ac = 12 + castLevel;
                    let hp = Math.max(30, 10 * castLevel);
                    let dc = targetActor.data.data.attributes.spelldc;
                    let spakMod = dc - 8;
                    await summonedTokenActor.update({
                        "data.attributes.ac.value": ac,
                        "data.attributes.hp.value": hp,
                        "data.attributes.hp.max": hp
                        });//here you can change actorData like strength on creation
                    let multiattack = duplicate(summonedTokenActor.items.find(i => i.name === "Multiattack"));
                    let shortsword = duplicate(summonedTokenActor.items.find(i => i.name === "Shortsword"));
                    let step = duplicate(summonedTokenActor.items.find(i => i.name.includes(mood)));
                    multiattack.data.description.value = multiattack.data.description.value.replace("a number of ", Math.floor(castLevel * 0.5) + " ");
                    shortsword.data.attackBonus = spakMod;
                    shortsword.data.damage.parts[0][0] += " + " + castLevel;
                    step.data.save.dc = dc;
                    await summonedTokenActor.updateEmbeddedDocuments("Item", [multiattack, shortsword, step]);
                    
                    if (removeTemplates) await canvas.scene.deleteEmbeddedDocuments("MeasuredTemplate", templateArray);
                }
            }
        },
    },
  }).render(true);
    
}

if(args[0] === "off"){
    let tokenId = DAE.getFlag(targetActor, uEffectName);
    canvas.scene.deleteEmbeddedDocuments("Token", [tokenId]);
    DAE.unsetFlag(targetActor, uEffectName);
}
